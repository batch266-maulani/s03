package com.zuitt.example;

public class LoopingStatements {
    public static void main(String[] args) {

        /*int x = 0;

        while (x<10){
            System.out.println("Loop Number: " + x++);
        }*/

     /*   int y = 10;

        do{
            System.out.println("Loop Number: " + y--);
        }while(y>=0);
    */
        /*for(int i = 0; i<10;i++){
            System.out.println("Current Court: " + i);
        }*/


        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        for(int i = 0; i< classroom.length; i++){
            for(int j = 0;j<3; j++){
                System.out.println(classroom[i][j]);
            }
        }






    }
}
