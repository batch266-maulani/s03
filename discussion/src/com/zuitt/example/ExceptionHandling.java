package com.zuitt.example;

import javax.sound.midi.Soundbank;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {


    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int num1,num2;



        try {
            System.out.println("Enter a number1: ");
            num1 = input.nextInt();
            System.out.println("Enter a number2: ");
            num2 = input.nextInt();

            System.out.println("The quotient is: " + num1/num2);
        }catch (ArithmeticException e){
            System.out.println("You cannot divide a whole number to 0!");
        }catch (InputMismatchException e){
            System.out.println("Please Input numbers only.");
        }
        catch (Exception e){
            System.out.println("Invalid Input!");
        } finally {
            System.out.println("This will execute no matter what");
        }

    }


}
